import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViewEmployeesComponent } from './component/view-employees/view-employees.component';
import { AddEmployeeComponent } from './component/add-employee/add-employee.component';
import { UpdateEmployeeComponent } from './component/update-employee/update-employee.component';
import { EmployeeDetailsComponent } from './component/employee-details/employee-details.component';
import { HeaderComponent } from './component/header/header.component';


import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { EmployeeService } from './service/employee.service';
import { SupervisorService } from './service/supervisor.service';
import { AddSupervisorComponent } from './component/add-supervisor/add-supervisor.component';
import { UpdateSupervisorComponent } from './component/update-supervisor/update-supervisor.component';
import { ViewSupervisorsComponent } from './component/view-supervisors/view-supervisors.component';
import { SupervisorDetailsComponent } from './component/supervisor-details/supervisor-details.component';



@NgModule({
  declarations: [
    AppComponent,
    ViewEmployeesComponent,
    AddEmployeeComponent,
    UpdateEmployeeComponent,
    EmployeeDetailsComponent,
    HeaderComponent,
    AddSupervisorComponent,
    UpdateSupervisorComponent,
    ViewSupervisorsComponent,
    SupervisorDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatMenuModule,
    MatSelectModule,
    MatSortModule,
    MatCardModule
  ],
  providers: [
    EmployeeService,
    SupervisorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

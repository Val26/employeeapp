import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employees } from '../domain/employees';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  
  private url = "http://localhost:8080/employees";
  
  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employees[]>{
    return this.http.get<Employees[]>(this.url);
  }

  addEmployee(employee: Employees): Observable<Employees>{
    return this.http.post<Employees>(this.url, employee);

  }

  deleteEmployee(id: number): Observable<number>{
    return this.http.delete<number>(this.url + '?id=' + id);
  }

  updateEmployee(id: number, employee: Employees): Observable<Employees>{
    return this.http.put<Employees>(this.url + '?id=' + id, employee);

  }

  getEmployeeById(id: number): Observable<Employees>{
    return this.http.get<Employees>(this.url + '/view?id=' + id);
  }

  getEmployeesBySupervisorId(id: number): Observable<Employees[]>{
    return this.http.get<Employees[]>(this.url + '/sId?id=' + id);
  }

  

  
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Supervisors } from '../domain/supervisors';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SupervisorService {

  private url = "http://localhost:8080/supervisors";

  constructor(private http: HttpClient) { }

  getSupervisors(): Observable<Supervisors[]>{
    return this.http.get<Supervisors[]>(this.url);
  }

  xSupervisor(supervisor: Supervisors): Observable<Supervisors>{
    return this.http.post<Supervisors>(this.url, supervisor);
  }

  deleteSupervisor(id: number): Observable<number>{
    return this.http.delete<number>(this.url + '?idSuper=' + id);
  }

  updateSupervisor(id: number, supervisor: Supervisors): Observable<Supervisors>{
    return this.http.put<Supervisors>(this.url + '?idSuper=' + id, supervisor);

  }

  getSupervisorById(id: number): Observable<Supervisors>{
    return this.http.get<Supervisors>(this.url + '/view?idSuper=' + id);
  }
}

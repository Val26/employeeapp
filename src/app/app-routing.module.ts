import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewEmployeesComponent } from './component/view-employees/view-employees.component';
import { AddEmployeeComponent } from './component/add-employee/add-employee.component';
import { UpdateEmployeeComponent } from './component/update-employee/update-employee.component';
import { EmployeeDetailsComponent } from './component/employee-details/employee-details.component';
import { ViewSupervisorsComponent } from './component/view-supervisors/view-supervisors.component';
import { AddSupervisorComponent } from './component/add-supervisor/add-supervisor.component';
import { UpdateSupervisorComponent } from './component/update-supervisor/update-supervisor.component';
import { SupervisorDetailsComponent } from './component/supervisor-details/supervisor-details.component';


const routes: Routes = [
  {
    path: '', component: ViewEmployeesComponent
  },
  {
    path: 'add', component: AddEmployeeComponent
  },
  {
    path: 'edit/:id', component: UpdateEmployeeComponent
  },
  {
    path: 'details/:id', component: EmployeeDetailsComponent
  },
  {
    path: 'viewS', component: ViewSupervisorsComponent
  },
  {
    path: 'addS', component: AddSupervisorComponent
  },
  {
    path: 'viewS/editS/:id', component: UpdateSupervisorComponent
  },
  {
    path: 'viewS/detailsS/:id', component: SupervisorDetailsComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

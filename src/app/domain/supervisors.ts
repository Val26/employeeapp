export class Supervisors {
    public idSuper: number;
    public firstNameSuper: string;
    public lastNameSuper: string;
    public gender: string;
}

import { Supervisors } from './supervisors';

export class Employees {
    public id: number;
    public firstName: string;
    public lastName: string;
    public gender: string;
    public salary: number;
    public resigned: boolean;
    public supervisor: Supervisors;
}

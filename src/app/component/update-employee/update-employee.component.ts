import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from 'src/app/service/employee.service';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { ActivatedRoute } from '@angular/router';
import { Supervisors } from 'src/app/domain/supervisors';
import { Employees } from 'src/app/domain/employees';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  id: number;

  employeeForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    salary: new FormControl(''),
    resigned: new FormControl(''),
    supervisor: new FormControl('')
  });

  constructor(
    private employeeService: EmployeeService,
    private supervisorService: SupervisorService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar) { }

    supervisors: Supervisors[] = [];

    editEmployee: Employees;

  ngOnInit() {
    this.supervisorService.getSupervisors()
    .subscribe(s=> {
      this.supervisors = s;
    });

    this.id = +this.route.snapshot.paramMap.get('id');

    this.employeeService.getEmployeeById(+this.id)
    .subscribe(employee=> {
      this.editEmployee = employee
      console.log(this.editEmployee)

    this.employeeForm.controls['firstName'].setValue(this.editEmployee.firstName);
    this.employeeForm.controls['lastName'].setValue(this.editEmployee.lastName);
    this.employeeForm.controls['gender'].setValue(this.editEmployee.gender);
    this.employeeForm.controls['salary'].setValue(this.editEmployee.salary);
    this.employeeForm.controls['resigned'].setValue(this.editEmployee.resigned);

    })
  }

  xeditEmployee(){
    this.editEmployee= {
      id:0,
      firstName: this.employeeForm.controls['firstName'].value,
      lastName: this.employeeForm.controls['lastName'].value,
      gender: this.employeeForm.controls['gender'].value,
      salary: this.employeeForm.controls['salary'].value,
      resigned: this.employeeForm.controls['resigned'].value,
      supervisor:{
        idSuper: +this.employeeForm.controls['supervisor'].value,
        firstNameSuper: '',
        lastNameSuper: '',
        gender:'',

      }
    }

    this.employeeService.updateEmployee(this.id, this.editEmployee)
    .subscribe(employee=> {
      console.log(employee);
      this.employeeForm.reset();
      this.snackBar.open(
        'Employee Successfully updated!',
        'Close', 
        {
          duration: 10000,
          verticalPosition: 'top'
        });
    });
  }

}

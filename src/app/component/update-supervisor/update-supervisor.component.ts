import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Supervisors } from 'src/app/domain/supervisors';

@Component({
  selector: 'app-update-supervisor',
  templateUrl: './update-supervisor.component.html',
  styleUrls: ['./update-supervisor.component.css']
})
export class UpdateSupervisorComponent implements OnInit {

  id: number;

  supervisorForm = new FormGroup({
    firstNameSuper: new FormControl(''),
    lastNameSuper: new FormControl(''),
    gender: new FormControl('')
  });

  constructor(
    private supervisorService: SupervisorService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar) { }

    editSupervisor: Supervisors;

  ngOnInit() {

    this.id = +this.route.snapshot.paramMap.get('id');

    this.supervisorService.getSupervisorById(+this.id)
    .subscribe(supervisor=> {
      this.editSupervisor = supervisor
      console.log(this.editSupervisor)
   

    this.supervisorForm.controls['firstNameSuper'].setValue(this.editSupervisor.firstNameSuper);
    this.supervisorForm.controls['lastNameSuper'].setValue(this.editSupervisor.lastNameSuper);
    this.supervisorForm.controls['gender'].setValue(this.editSupervisor.gender);
  })
}

  xeditSupervisor(){
    this.editSupervisor= {
      idSuper:0,
      firstNameSuper: this.supervisorForm.controls['firstNameSuper'].value,
      lastNameSuper: this.supervisorForm.controls['lastNameSuper'].value,
      gender: this.supervisorForm.controls['gender'].value,
  }

  this.supervisorService.updateSupervisor(this.id, this.editSupervisor)
    .subscribe(supervisor=> {
      console.log(supervisor);
      this.supervisorForm.reset();
      this.snackBar.open(
        'Supervisor Successfully updated!',
        'Close', 
        {
          duration: 10000,
          verticalPosition: 'top'
        });
    });
  }
}

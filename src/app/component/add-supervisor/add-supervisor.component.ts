import { Component, OnInit } from '@angular/core';
import { Supervisors } from 'src/app/domain/supervisors';
import { FormGroup, FormControl } from '@angular/forms';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-supervisor',
  templateUrl: './add-supervisor.component.html',
  styleUrls: ['./add-supervisor.component.css']
})
export class AddSupervisorComponent implements OnInit {

  ss:Supervisors = null;

  supervisorForm = new FormGroup({
    firstNameSuper: new FormControl(''),
    lastNameSuper: new FormControl(''),
    gender: new FormControl('')
  });

  constructor(private supervisorService: SupervisorService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  aaddSupervisor(){
    this.ss = {
      idSuper:0,
      firstNameSuper:this.supervisorForm.value.firstNameSuper,
      lastNameSuper:this.supervisorForm.value.lastNameSuper,
      gender:this.supervisorForm.value.gender
      
      };

      this.supervisorService.xSupervisor(this.ss)
        .subscribe(s=>{
          this.supervisorForm.reset();
          this.snackBar.open(
            'Supervisor Successfully added!',
            'Close', 
            {
              duration: 10000,
              verticalPosition: 'top'
            });
        });

  }
  }

import { Component, OnInit, ViewChild } from '@angular/core';
import { Supervisors } from 'src/app/domain/supervisors';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-view-supervisors',
  templateUrl: './view-supervisors.component.html',
  styleUrls: ['./view-supervisors.component.css']
})
export class ViewSupervisorsComponent implements OnInit {

  supervisors: Supervisors[] = [];
  dataSource = new MatTableDataSource<Supervisors>();
  displayedColumns = ['idSuper', 'firstNameSuper', 'lastNameSuper', 'gender', 'action'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private supervisorService: SupervisorService) { }

  ngOnInit() {
    
    this.supervisorService.getSupervisors()
    .subscribe(s =>{  console.log(s)
      this.supervisors = s;
      this.dataSource.data = this.supervisors;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    
  }

  deleteSupervisor(id: number){
    console.log(id)
    this.supervisorService.deleteSupervisor(id)
    .subscribe(s=> window.location.reload());
  }

  

}

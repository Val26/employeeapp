import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/service/employee.service';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { Supervisors } from 'src/app/domain/supervisors';
import { FormGroup, FormControl } from '@angular/forms';
import { Employees } from 'src/app/domain/employees';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
 
  ee:Employees = null;


  employeeForm = new FormGroup({
    id: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    salary: new FormControl(''),
    resigned: new FormControl(''),
    supervisor: new FormControl('')

  })

  constructor(
    private employeeService: EmployeeService,
    private supervisorService: SupervisorService,
    private snackBar: MatSnackBar) { }

    supervisors: Supervisors[] = [];

  ngOnInit() {
    this.supervisorService.getSupervisors()
    .subscribe(s => {
      console.log(s);
      this.supervisors = s;
    })
  }

  addEmployee(){
    console.log(this.employeeForm.value.supervisor);
    this.ee = {
      id:+this.employeeForm.value.id,
      firstName:this.employeeForm.value.firstName,
      lastName:this.employeeForm.value.lastName,
      gender:this.employeeForm.value.gender,
      salary:+this.employeeForm.value.salary,
      resigned:this.employeeForm.value.resigned,
      supervisor:{
        idSuper:+this.employeeForm.value.supervisor,
        firstNameSuper:"",
        lastNameSuper:"",
        gender:""}
      };


    this.employeeService.addEmployee(this.ee)
    .subscribe(employee=>{
      this.employeeForm.reset();
      this.snackBar.open(
        'Employee Successfully added!',
        'Close', 
        {
          duration: 10000,
          verticalPosition: 'top'
        });
    });

  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Supervisors } from 'src/app/domain/supervisors';
import { Employees } from 'src/app/domain/employees';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { Route } from '@angular/compiler/src/core';
import { EmployeeService } from 'src/app/service/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-supervisor-details',
  templateUrl: './supervisor-details.component.html',
  styleUrls: ['./supervisor-details.component.css']
})
export class SupervisorDetailsComponent implements OnInit {

  id: number;
  supervisorDetails: Supervisors;
  employeeList: Employees[];

  dataSource = new MatTableDataSource<Employees>();
  displayedColumns = ['id', 'firstName', 'lastName', 'gender', 'salary', 'resigned'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private employeeService: EmployeeService,
    private supervisorService: SupervisorService,
    private route: ActivatedRoute, private router: Router)
     { }

  ngOnInit() {
    this.supervisorDetails = new Supervisors();
    this.id = this.route.snapshot.params['id'];
    this.supervisorService.getSupervisorById(this.id)
    .subscribe(data => {
      console.log(data)
      this.supervisorDetails = data;
      
    }, error => console.log(error));


      this.employeeService.getEmployeesBySupervisorId(this.id).subscribe(emp=>{
        this.employeeList=emp;
        this.dataSource.data = this.employeeList;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(this.employeeList)
      });
  }
}

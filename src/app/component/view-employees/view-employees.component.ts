import { Component, OnInit, ViewChild } from '@angular/core';
import { Employees } from 'src/app/domain/employees';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { EmployeeService } from 'src/app/service/employee.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-view-employees',
  templateUrl: './view-employees.component.html',
  styleUrls: ['./view-employees.component.css']
})
export class ViewEmployeesComponent implements OnInit {

  employees: Employees[] = [];
  dataSource = new MatTableDataSource<Employees>();
  displayedColumns = ['id', 'firstName', 'lastName', 'gender', 'salary', 'resigned', 'supervisor', 'action'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private employeeService: EmployeeService) { }

  ngOnInit() {
    
    this.employeeService.getEmployees()
    .subscribe(employees =>{ 
      this.employees = employees;
      this.dataSource.data = this.employees;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    
  }

  deleteEmployee(id: number){
    this.employeeService.deleteEmployee(id)
    .subscribe(s=> window.location.reload());
  }

  // this.employee = new Employees();

  //   this.id = this.route.snapshot.params['id'];
    
  //   this.employeeService.getEmployee(this.id)
  //     .subscribe(data => {
  //       console.log(data)
  //       this.employee = data;
  //     }, error => console.log(error));
  // }

  // list(){
  //   this.router.navigate(['employees']);
  // }






}
